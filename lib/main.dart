import 'dart:ui' as prefix0;

import 'package:drink_quizz/src/ui/mainscreenquizzmode.dart';
import 'package:flutter/material.dart';
import 'src/ui/select_player.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primaryColor: Color.fromARGB(255, 29, 93, 138),
          accentColor: Color.fromARGB(255, 236, 150, 10),
          backgroundColor: Color.fromARGB(255, 29, 112, 169)),
      home: Builder(
          builder: (context) => Scaffold(
                backgroundColor: Theme.of(context).backgroundColor,
                appBar: AppBar(
                  title: Text('Drink Quizz Home Page'),
                ),
                body: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Select a game mode',
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      RaisedButton(
                          child: Text("Start a Quizz"),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MainScreenQuizzMode()));
                          }),
                      SizedBox(
                        height: 20,
                      ),
                      RaisedButton(
                          child: Text("Drink mode"),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SelectPlayer()));
                          })
                    ],
                  ),
                ),
                //floatingActionButton: FloatingActionButton(
                //  onPressed: _incrementCounter,
                //  tooltip: 'Increment',
                //  child: Icon(Icons.add),
                //), // This trailing comma makes auto-formatting nicer for build methods.
              )),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Select a game mode',
            ),
            SizedBox(
              height: 20,
            ),
            RaisedButton(
                child: Text("Quizz mode"),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SelectPlayer()));
                }),
            SizedBox(
              height: 20,
            ),
            RaisedButton(
                child: Text("Drink mode"),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SelectPlayer()));
                })
          ],
        ),
      ),
      //floatingActionButton: FloatingActionButton(
      //  onPressed: _incrementCounter,
      //  tooltip: 'Increment',
      //  child: Icon(Icons.add),
      //), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
