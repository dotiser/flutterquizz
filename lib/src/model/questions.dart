import 'package:html_unescape/html_unescape.dart';

class Questions {
  String category;
  String type;
  String difficulty;
  String question;
  String correctAnswer;
  List<String> answers;
  String answered;
  bool rightAnswer = false;

  Questions(questionJson) {
    category = questionJson['category'];
    type = questionJson['type'];
    difficulty = questionJson['difficulty'];
    question = new HtmlUnescape().convert(questionJson['question']);
    correctAnswer = new HtmlUnescape().convert(questionJson['correct_answer']);
    List<String> temp = [];
    for (int i = 0; i < questionJson['incorrect_answers'].length; i++) {
      String incorrectAnswer =
          new HtmlUnescape().convert(questionJson['incorrect_answers'][i]);
      temp.add(incorrectAnswer);
    }
    temp.add(correctAnswer);
    answers = temp;
  }
}
