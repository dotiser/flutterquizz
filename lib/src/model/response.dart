import 'package:drink_quizz/src/model/questions.dart';

class Response {
  int responseCode = -1;
  List<Questions> questionsList = new List(0);

  Response();

  Response.fromJson(Map<String, dynamic> parsedJson) {
    responseCode = parsedJson['response_code'];
    List<Questions> temp = [];
    for (int i = 0; i < parsedJson['results'].length; i++) {
      Questions result = Questions(parsedJson['results'][i]);
      temp.add(result);
    }
    questionsList = temp;
  }
}