import 'dart:async';

import '../model/response.dart';
import 'questions_api_provider.dart';

class Repository {
  final moviesApiProvider = QuestionsApiProvider();

  Future<Response> fetchQuestionList() => moviesApiProvider.fetchQuestionList();
}
