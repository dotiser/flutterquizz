import 'dart:async';
import 'package:http/http.dart' show Client;
import 'dart:convert';
import '../model/response.dart';

class QuestionsApiProvider {
  Client client = Client();

  Future<Response> fetchQuestionList() async {
    print("QuestionsApiProvider - fetchQuestionList");
    final response = await client
        .get("https://opentdb.com/api.php?amount=3");
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return Response.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load questions list');
    }
  }
}