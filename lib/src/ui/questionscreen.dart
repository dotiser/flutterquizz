import 'package:drink_quizz/src/model/questions.dart';
import 'package:drink_quizz/src/ui/responsegrid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class QuestionScreen extends StatefulWidget {
  final Questions question;
  final int numberQuestion;
  final VoidCallback onNextScreenCalled;

  QuestionScreen({this.numberQuestion, this.question, this.onNextScreenCalled});

  _QuestionScreenState createState() => _QuestionScreenState();
}

class _QuestionScreenState extends State<QuestionScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 20, 10, 0),
      padding: EdgeInsets.all(10.0),
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Text("Question number # " + widget.numberQuestion.toString(),
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w700)),
          SizedBox(
            height: 20,
          ),
          Text("${widget.question.question}"),
          SizedBox(
            height: 20,
          ),
          SizedBox(
              height: 200,
              child: ResponseGrid(
                question: this.widget.question,
                onResponseChanged: (String response) {
                  setState(() => true);
                },
              )),
          if (widget.question.answered != null) _buildConditionnalBloc()
        ],
      ),
    );
  }

  Widget _buildConditionnalBloc() {
    String textDisplay = "";
    if (widget.question.answered == widget.question.correctAnswer) {
      textDisplay = "Your answer is correct";
    } else {
      textDisplay = "Your answer is wrong. Response was ${widget.question.correctAnswer}";
    }

    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(textDisplay),
          SizedBox(
            height: 20,
          ),
          Container(
              margin: EdgeInsets.all(10.0),
              height: 50.0,
              width: 200.0,
              decoration: BoxDecoration(color: Colors.blue, boxShadow: [
                BoxShadow(color: Colors.grey, blurRadius: 4, offset: Offset(0, 2))
              ]),
              child: Material(
                color: Theme.of(context).primaryColor,
                child: InkWell(
                  child: Center(
                      child: Text(
                        "Next question",
                        style: TextStyle(fontSize: 20.0, color: Colors.white),
                      )),
                  onTap: () {
                    widget.onNextScreenCalled();
                  },
                ),
              ))
        ],
        mainAxisAlignment: MainAxisAlignment.center);
  }

}
