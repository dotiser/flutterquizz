import 'package:drink_quizz/src/model/questions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ResponseGrid extends StatefulWidget {
  final Questions question;
  final Function(String) onResponseChanged;

  ResponseGrid({this.question, this.onResponseChanged});

  _ResponseGridState createState() => _ResponseGridState();
}

class _ResponseGridState extends State<ResponseGrid> {
  _onSelected(int index) {
    if (widget.question.answered == null) {
      widget.question.answered = widget.question.answers[index];
      widget.question.rightAnswer =
          widget.question.answers[index] == widget.question.correctAnswer;
      widget.onResponseChanged(widget.question.answers[index]);
      setState(() => true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: widget.question.answers.length,
      itemBuilder: _buildGridItems,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 2,
          mainAxisSpacing: 10,
          crossAxisSpacing: 10),
    );
  }

  Widget _buildGridItems(BuildContext context, int index) {
    return GestureDetector(
      onTap: () => _onSelected(index),
      child: GridTile(
          child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 0.5)),
              child: Material(
                  color: widget.question.rightAnswer &&
                      widget.question.answers[index] == widget.question.answered
                      ? Colors.green
                      : widget.question.answers[index] == widget.question.answered
                      ? Colors.red
                      : Colors.transparent,
                  child: InkWell(
                child: Center(
                  child: Text(widget.question.answers[index], overflow: TextOverflow.clip, maxLines: 3,textAlign: TextAlign.center,),
                ),
              )))),
    );
  }
}
