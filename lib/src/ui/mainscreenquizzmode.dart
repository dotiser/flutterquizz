import 'package:drink_quizz/src/model/questions.dart';
import 'package:drink_quizz/src/model/response.dart';
import 'package:drink_quizz/src/ui/navigatorquestionscreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../resources/repository.dart';

class MainScreenQuizzMode extends StatefulWidget {
  MainScreenQuizzMode({Key key}) : super(key: key);

  @override
  _MainScreenQuizzModeState createState() => _MainScreenQuizzModeState();
}

class _MainScreenQuizzModeState extends State<MainScreenQuizzMode> {
  Future<Response> futureResponse;
  List<Questions> questionsList;
  Repository repository;

  @override
  void initState() {
    super.initState();
    repository = Repository();
    futureResponse = repository.fetchQuestionList();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Quizz mode',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Quizz mode'),
        ),
        body: Center(
          child: FutureBuilder<Response>(
            future: futureResponse,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text("Loading quizz questions")
                  ],
                );
              } else if (snapshot.hasError) {
                return new Text('Error: ${snapshot.error}');
              } else {
                final items = snapshot.data ??
                    new Response(); // handle the case that data is null
                if (items.responseCode == 0) {
                  questionsList = items.questionsList;
                  return NavigatorQuestionScreen(
                    questionsList: questionsList,
                    startNewGame: () {
                      setState(() {
                        futureResponse = repository.fetchQuestionList();
                      });
                    },
                  );
                  //return QuestionScreen(numberQuestion : 0, question : snapshot.data.questionsList[0], onNextScreenCalled: () {_handleNextScreen()},);
                } else {
                  return Text("Error while getting list of questions");
                }
              }
            },
          ),
        ),
      ),
    );
  }
}
