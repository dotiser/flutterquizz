import 'package:flutter/material.dart';

class SelectPlayer extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primaryColor: Color.fromARGB(255, 29, 93, 138),
          accentColor: Color.fromARGB(255, 236, 150, 10),
          backgroundColor: Color.fromARGB(255, 29, 112, 169)
      ),
      home: SelectPlayerPage(title: 'Drink Quizz Select player'),
    );
  }
}

class SelectPlayerPage extends StatefulWidget {
  SelectPlayerPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SelectPlayerPageState createState() => _SelectPlayerPageState();
}


class _SelectPlayerPageState extends State<SelectPlayerPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme
          .of(context)
          .backgroundColor,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Select a number of player',
            ),
            SizedBox(height: 20),
            RaisedButton(key: Key("gameMode2"), onPressed: () {
              Navigator.pop(context);
            }, child: Text(
                'Add a player',
                style: TextStyle(fontSize: 20))),
          ],
        ),
      ),
    );
  }
}