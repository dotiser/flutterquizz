import 'package:drink_quizz/src/model/questions.dart';
import 'package:drink_quizz/src/ui/questionscreen.dart';
import 'package:flutter/material.dart';

class NavigatorQuestionScreen extends StatefulWidget {
  final List<Questions> questionsList;
  final VoidCallback startNewGame;

  NavigatorQuestionScreen({Key key, this.questionsList, this.startNewGame}) : super(key: key);

  @override
  _NavigatorQuestionScreenState createState() =>
      _NavigatorQuestionScreenState();
}

class _NavigatorQuestionScreenState extends State<NavigatorQuestionScreen> {
  int numberQuestion = 0;
  int numberCorrectAnswers = 0;
  bool gameOver = false;

  Widget build(BuildContext context) {
    if (gameOver) {
      return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text("Game Over!"),
            Text("You answered to ${numberQuestion + 1} questions with " +
                _calculateNumberCorrectAnswers().toString() +
                " corrects answers"),
            SizedBox(height: 30),
            Container(
                margin: EdgeInsets.all(10.0),
                height: 50.0,
                width: 200.0,
                decoration: BoxDecoration(color: Colors.blue, boxShadow: [
                  BoxShadow(
                      color: Colors.grey, blurRadius: 4, offset: Offset(0, 2))
                ]),
                child: Material(
                  color: Theme.of(context).primaryColor,
                  child: InkWell(
                    child: Center(
                        child: Text(
                      "Start a new game",
                      style: TextStyle(fontSize: 20.0, color: Colors.white),
                    )),
                    onTap: () {widget.startNewGame();},
                  ),
                ))
          ]);
    } else {
      return QuestionScreen(
        numberQuestion: numberQuestion + 1,
        question: widget.questionsList[numberQuestion],
        onNextScreenCalled: () {
          _handleNextScreen();
        },
      );
    }
  }

  int _calculateNumberCorrectAnswers() {
    int numberCorrect = 0;
    widget.questionsList.forEach((Questions q) =>
        {if (q.answered != null && q.rightAnswer) numberCorrect++});
    return numberCorrect;
  }

  void _handleNextScreen() {
    setState(() {
      if (numberQuestion == widget.questionsList.length - 1) {
        numberCorrectAnswers = _calculateNumberCorrectAnswers();
        gameOver = true;
      } else {
        numberQuestion++;
      }
    });
  }
}
